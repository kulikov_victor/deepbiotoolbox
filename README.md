# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The goal is to develop a toolbox that would allow users to handle the sheer variety of input data (variable cell types and microscopy modalities, variability in the size of training data, different equipment and image acquisition parameters, etc.), while allowing attaining highly accurate segmentation with little supervision.
* Version 0.0.1

### How do I get set up? ###

* Dependencies

* numpy 1.12
* skimage
* tensorflow 1.0.1
* keras 2.0.2
* pandas
* matplotlib

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines