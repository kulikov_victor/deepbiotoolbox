import tensorflow as tf
from keras import backend as K
from keras.layers import Dropout
from keras.layers import Input, MaxPooling2D, Conv2D
from keras.models import Model
from keras.layers.core import Activation
from keras.layers import Lambda
from data.sets import DeepBioData,threadsafe_generator

from segmentation.networks import SegNet
import numpy as np


class ClassifyNet(SegNet):
    """
    Base class for fully convolutional classification 
    """
    def __init__(self,dataset, weights_file='', verbose=False, log_device_placement=False, sess=None):
        SegNet.__init__(self,dataset=dataset,weights_file=weights_file,verbose=verbose,
                        log_device_placement=log_device_placement,sess=sess)

        self.downscaling_number = 3


    @staticmethod
    def convolution_downscaling(inp,i):
        convolution = Conv2D(32*(2**i), (3, 3), activation='relu',dilation_rate=(3, 3), padding='same')(inp)
        convolution = Dropout(0.2)(convolution)
        convolution = Conv2D(32*(2**i), (3, 3), activation='relu',dilation_rate=(3, 3), padding='same')(convolution)
        return MaxPooling2D(pool_size=(2, 2))(convolution)

    def create_model(self, patch_height, patch_width):
        input_data = Input((patch_height, patch_width, self.n_channels), name='x')
        tensor = input_data
        for i in range(self.downscaling_number):
            tensor = self.convolution_downscaling(tensor,i)

        tensor = Conv2D(self.n_labels, (1, 1), activation='relu', padding='same')(tensor)

        mean_layer = Lambda(lambda xin: K.mean(xin, axis=[1,2]), output_shape=(self.n_labels,))
        tensor = mean_layer(tensor)
        activated = Activation('softmax', name='y')(tensor)

        model = Model(inputs=input_data, outputs=activated)

        if self.verbose:
            model.summary()

        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.name = "FCC"
        return model

    def process_image(self, image, probabilities=False):
        """
        Process one whole image

        :param image: the image to be processed
        :param probabilities: returns raw probabilities, of colors
        :return: 
        """
        assert len(image.shape)==2

        image = image[:,:,np.newaxis]
        self.batch_size = 1

        if (self.previous_shape is None) or (self.previous_shape != image.shape):
            self.model = self.create_model(image.shape[0], image.shape[1])
            self.previous_shape = image.shape
            # try to load model weights
            if self.model_weights is not None:
                self.set_model_weights(self.model, self.model_weights)
            elif self.weights_file:
                self.model.load_weights(self.weights_file + '.h5')
            else:
                raise "Can't find model weights"

        image = (image - np.mean(self.dataset.pixel_mean)) / np.mean(self.dataset.max_value)
        image = image[np.newaxis, :, :, :]

        y_predicted = self.model.predict(image)
        return y_predicted[0]


class ClassifyChannels(ClassifyNet):
    def __init__(self, dataset, weights_file='', verbose=False, log_device_placement=False, sess=None):
        assert isinstance(dataset, DeepBioData)
        ClassifyNet.__init__(self,dataset=dataset,weights_file=weights_file,verbose=verbose,
                        log_device_placement=log_device_placement,sess=sess)
        self.dataset = dataset
        self.batch_size = 1
        self.n_channels = 1
        self.downscaling_number = 3
        self.n_labels = self.dataset.ndims
        self.batch_factor = self.n_labels

    def _create_train_generator(self,n_files):
        return self.generate(n_files,self.dataset.train, self.dataset.train_labels)

    def _create_test_generator(self,n_files):
        return self.generate(n_files,self.dataset.test, self.dataset.test_labels)

    @threadsafe_generator
    def generate(self, n_files,images,labels):
        while 1:
            [data, _] = self.dataset._abstract_generator(n_files, images, labels)
            for i in range(data.shape[0]):
                data[i] = (data[i]*self.dataset.max_value+self.dataset.pixel_mean)
                data[i] = (data[i] - np.mean(self.dataset.pixel_mean)) / np.mean(self.dataset.max_value)

            channel_data = np.concatenate(np.split(data, self.n_labels, axis=3),axis=0)
            eye =np.eye(self.n_labels,dtype=np.float32)
            classes = np.repeat(eye, data.shape[0], axis=0)
            yield ({'x': channel_data}, {'y': classes})



