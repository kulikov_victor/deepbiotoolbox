import unittest

from data.sets import Hoechst
from classification.networks import ClassifyChannels
from data.augmentation import *

class TestNetwork:
    def create_network(self):
        raise "Not implemented"

    def test_train(self):
        net = self.create_network()
        net.train(2, augmentation_list, steps_per_epoch=10, epochs=1)

    def test_testing(self):
        net = self.create_network()
        print(net.test(augmentation_list,ax=1))

    def test_fullimage(self):
        net = self.create_network()
        net.process_image(test_dataset.test[0][:,:,4])


class TestClassifyNetwork(unittest.TestCase, TestNetwork):
    def create_network(self):
        return ClassifyChannels(test_dataset,weights_file='../models/Class_hoechst')

if __name__ == '__main__':
    test_dataset = Hoechst(0.9)
    augmentation_list = [Affine(), ClipPatches(), ElasticTransform(),Flip()]
    unittest.main()
