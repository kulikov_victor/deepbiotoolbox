"""
@@Ecoli
@@Drosophila
@@PickleDataset
@@BBBC
"""
import numpy as np
from data import sets
from data import  augmentation

from sets import Ecoli
from sets import Drosophila
from sets import PickleDataset
from sets import BBBC
from sets import Warwick

from augmentation import Affine
from augmentation import ElasticTransform
from augmentation import Contrast
from augmentation import ClipPatches
from augmentation import Flip