# Copyright Victor Kulikov 2017 Skoltech. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =============================================================================


import numpy as np
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.interpolation import map_coordinates
from skimage import exposure
from skimage import transform


class Augmentation:
    """
    Base class for augmentations. Augmentations can be added to Dataset or used separatly.
    
    After apply each image is converted into NxWxHxC format
    """

    def __init__(self):
        self.out_shape = None
        self.n_channels = 1
        self.n_images = 1

    def prepare(self, random_state=None):
        """
        Calculate common parameter of the augmentation
        
        :param random_state: seed to generate random
        :return: 
        """
        pass

    def apply(self, image):
        pass

    def get_output_shape(self):
        """
        return the prdicted shape of the output array
        """
        return self.out_shape
    
    def parse_image(self,image):
        """
        Converts the image to NxWxHxC format
        """
        if 2 == len(image.shape):
            image = image[np.newaxis,:, :, np.newaxis]
            
        if 3 == len(image.shape):
            image = image[np.newaxis,:, :, :]
            
        self.n_channels = image.shape[3]  
        
        return image
            
    def parse_shape(self,shape):
        """
        
        """
        
        assert isinstance(shape, tuple)
        assert len(shape) >= 2
        assert len(shape) <= 4
        self.n_images = 1
        self.n_channels = 1
        if 2 == len(shape):
            #Just image WxH
            self.out_shape = (1,shape[0],shape[1],1)
            self.shape = shape
        if 3 == len(shape):
            #Color image WxHxC
            self.out_shape = (1,shape[0],shape[1],shape[2])
            self.shape = shape[:2]
            self.n_channels = shape[2]
        if 4 == len(shape):
            #Tensor NxWxHxC
            self.out_shape = shape
            self.shape = shape[1:3]
            self.n_channels = shape[3]
            self.n_images  = shape[0]
            

class Affine(Augmentation):
    """
    Affine augmentation: translation, scaling and rotation
    
    Fills unknown regions with mean value.
    """

    def __init__(self, scale=(1.4, 0.8), shift_variance=5):
        """
        Creates a affine transformation augmentation
        
        :param shape: the shape of the image
        :param scale: a tuple of maximum scale and minimum scale
        :param shift_variance: maximum shift form center
        """
        self.out_shape = None
        assert isinstance(scale, tuple)
        self.scale = scale
        self.shift_variance = shift_variance
        self.final_transform = None

    def prepare(self, shape, random_state=None):
        self.parse_shape(shape)
        if random_state is None:
            random_state = np.random.RandomState(None)

        scale = (self.scale[1] - self.scale[0]) * random_state.rand() + self.scale[0]
        shift_y, shift_x = np.array(self.shape[:2]) / 2.
        rotate = transform.SimilarityTransform(rotation=np.deg2rad(random_state.rand() * 360), scale=(scale, scale))
        shift = transform.SimilarityTransform(translation=[-shift_x, -shift_y])
        shift_inv = transform.SimilarityTransform(
            translation=[shift_x + random_state.randint(-self.shift_variance, self.shift_variance),
                         shift_y + random_state.randint(-self.shift_variance, self.shift_variance)])

        self.final_transform = (shift + (rotate + shift_inv)).inverse

    def apply(self, image):

        assert (self.final_transform is not None)
        
        image = self.parse_image(image)
        assert (self.shape == image.shape[1:3])

        destination = np.zeros_like(image)
        mean_value = np.mean(image)
        

        
        for i in range(0, self.n_images):
            for j in range(0, self.n_channels):
                destination[i,:, :, j] = transform.warp(image[i,:, :, j], 
                           self.final_transform,cval=mean_value, 
                           preserve_range=True)
        

        return destination


class ElasticTransform(Augmentation):
    """
    Function calculate elastic transform on dense grid of the image 

    Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
    Convolutional Neural Networks applied to Visual Document Analysis", in
    Proc. of the International Conference on Document Analysis and
    Recognition, 2003.
    code from https://gist.github.com/chsasank/4d8f68caf01f041a6453e67fb30f8f5a

    :param shape: shape of elastic mash
    :type shape: list of int
    :param alpha: gain of the mesh nodes
    :type alpha: float
    :param sigma: variance of displacements
    :type sigma: float
    :param random_state: seed of the random function 
    :type random_state: int


    :return: 2d numpy array of indices displacement grid
    """

    def __init__(self, alpha=200.0, sigma=10.0):
        """
        :param shape: shape of elastic mash
        :type shape: numpy shape
        :param alpha: gain of the mesh nodes
        :type alpha: float
        :param sigma: variance of displacements
        :type sigma: float
        """
        assert isinstance(alpha, float)
        assert isinstance(sigma, float)
        self.out_shape = None
        self.alpha = alpha
        self.sigma = sigma
        self.indices = None

    def prepare(self,shape,random_state=None):

        self.parse_shape(shape)
        
        if random_state is None:
            random_state = np.random.RandomState(None)

        dx = gaussian_filter((random_state.rand(*self.shape) * 2 - 1), 
                             self.sigma, mode="constant", cval=0) * self.alpha
        dy = gaussian_filter((random_state.rand(*self.shape) * 2 - 1), 
                             self.sigma, mode="constant", cval=0) * self.alpha

        x, y = np.meshgrid(np.arange(self.shape[0]), np.arange(self.shape[1]), indexing='ij')
        self.indices = np.reshape(x + dx, (-1, 1)), np.reshape(y + dy, (-1, 1))
        return self.indices

    def apply(self, image):
        assert (self.indices is not None)
        
        image = self.parse_image(image)
        assert (self.shape == image.shape[1:3])

        mean_value = np.mean(image)
        destination = np.zeros_like(image)
        
        for i in range(0, self.n_images):
            for j in range(0, self.n_channels):
                destination[i,:, :, j] = map_coordinates(image[i,:, :, j], 
                           self.indices, cval=mean_value, order=1).reshape(
                                   self.shape)

        return destination


class ClipPatches(Augmentation):
    """
    Augmentation by subsampling
    """

    def __init__(self, patch_size=(128, 128), n_patch=10):
        """
        
        :param shape: the shape of the image len(shape) >=2
        :type shape: tuple
        :param patch_size: the size of the patch
        :type patch_size: tuple(int,int)
        :param n_patch: number of patches to be sampled
        :type n_patch: int
        """
        self.out_shape = None
        assert isinstance(patch_size, tuple)
        assert len(patch_size) == 2      
        assert n_patch >= 1
        
        

        self.patch_size = patch_size
        self.x = []
        self.y = []
        self.n_patch = n_patch

    def prepare(self,shape, random_state=None):
        """
        Calculate common parameter of the augmentation

        :param random_state: seed to generate random
        :return: 
        """
        self.parse_shape(shape)
        self.out_shape = (self.out_shape[0]*self.n_patch,self.patch_size[0],self.patch_size[1],self.n_channels)
        
        if random_state is None:
            random_state = np.random.RandomState(None)

        self.x = []
        self.y = []
        for i in range(0, self.n_patch):
            self.x.append(random_state.randint(0, self.shape[0] - self.patch_size[0]))
            self.y.append(random_state.randint(0, self.shape[1] - self.patch_size[1]))

    def apply(self, image):
        """

        :param image: the image to process
        :type image: numpy.ndarray
        :return: 
        """
        assert (len(self.x) > 0)
        assert (len(self.y) > 0)
        assert (len(self.y) == len(self.x))
        
        image = self.parse_image(image)
        assert (self.shape == image.shape[1:3])

        patches = np.ndarray(shape=[self.n_images*self.n_patch, self.patch_size[0], self.patch_size[1],
                                    self.n_channels], dtype=np.float32)

        
        # multiple channel image
        for i in range(0, self.n_images):
            for j in xrange(self.n_patch):
                patches[i*self.n_patch+j] = image[i,self.x[j]:self.x[j] + self.patch_size[0], self.y[j]:self.y[j] + self.patch_size[1],:]
     
        return patches


class Flip(Augmentation):
    """
    Flip transpose augmentation
    
    Apply random flip/transpose augmentation
    """

    def __init__(self, flip=True, transpose=True, force_flip=False, force_transpose=False):
        """
        
        :param shape: shape of the 
        :param flip: 
        :param transpose: 
        """
        assert isinstance(flip, bool)
        assert isinstance(transpose, bool)
        self.out_shape = None
        self.flip = flip
        self.transpose = transpose
        self.do_flip = 0
        self.do_transpose = 0
        self.force_flip = force_flip
        self.force_transpose = force_transpose

    def prepare(self,shape, random_state=None):
        self.parse_shape(shape)
        self.shape = shape
        self.do_flip = np.random.choice(2)
        self.do_transpose = np.random.choice(2)

    def apply(self, image):
        """

        :param image: the image to process
        :type image: numpy.ndarray
        :return: 
        """
        image = self.parse_image(image)        
        
        result = image
        if not self.force_flip:
            if self.flip and self.do_flip:
                 result = np.rot90(result, 2, axes=(1, 2))
        else:
            result = np.rot90(result, 2, axes=(1, 2))

        if not self.force_transpose:
            if self.transpose and self.do_transpose:
                result = np.rot90(result, 1, axes=(1, 2))
        else:
            result = np.rot90(result, 1, axes=(1, 2))

        return result


class Contrast(Augmentation):
    """
    Change image contrast add blur and noise
    
    This Augmentation adds blur, then correct gamma and than add variance
    """

    def __init__(self, noise_variance=0.2, gamma=0.3, blur=1.2):
        """
        
        :param shape: the shape of the image len(shape) >=2
        :type shape: tuple
        :param noise_variance: 
        :type noise_variance: float
        :param gamma: gamma parameter for gamma correction
        :type gamma: float
        :param blur: sigma of gaussian filter to convolve with the image the image
        :type blur: float
        """

        assert isinstance(noise_variance, float)
        assert isinstance(blur, float)
        assert isinstance(gamma, float)
        self.out_shape = None
        self.noise_variance = noise_variance
        self.gamma = gamma
        self.blur = blur
        self.noise = None

    def prepare(self,shape, random_state=None):
        """
        Calculate common parameter of the augmentation
        
        :param random_state: seed to generate random
        :return: 
        """
        self.parse_shape(shape)
        self.shape = shape
        
        if random_state is None:
            random_state = np.random.RandomState(None)

        self.noise = random_state.normal(0.0, self.noise_variance, size=self.out_shape)

    def apply(self, image):
        """
        
        :param image: the image to process
        :type image: numpy.ndarray
        :return: 
        """
        assert (self.noise is not None)
        image = self.parse_image(image)
        

        destination = gaussian_filter(image, self.blur, 0)
        destination = exposure.adjust_gamma(destination, self.gamma)
        if image.shape[3] == self.noise.shape[3]:
            assert image.shape== self.noise.shape
            destination += self.noise
                
        return destination
