import time
import unittest

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

from augmentation import Affine
from augmentation import ClipPatches
from augmentation import Contrast
from augmentation import ElasticTransform
from augmentation import Flip


def process(image, augmentation, labels=None):
    """
    Process an image with give augmentation
    
    :param image: 2d or 3d image
    :param augmentation: augmentation class
    :return: 
    """
    beg_ts = time.time()
    augmentation.prepare(image.shape)
    result = augmentation.apply(image)
    if labels is not None:
        labels_result = augmentation.apply(labels)
    end_ts = time.time()
    print("elapsed time: %f" % (end_ts - beg_ts))
    augmentation.get_output_shape()

    if verbose:
        fig = plt.figure(figsize=(10, 10))
        plt.imshow(result[0, :, :, 0], cmap='gray')
        fig.suptitle(augmentation.__class__.__name__)
    return result


def check_shape(image, result):
    if 2 == len(image.shape):
        assert ((1, image.shape[0], image.shape[1], 1) == result.shape)
    if 3 == len(image.shape):
        assert ((1, image.shape[0], image.shape[1], image.shape[2]) == result.shape)
    if 4 == len(image.shape):
        assert (image.shape == result.shape)


class AugTest:
    """
    Base class for automatic tests
    """

    def test_single_channel(self):
        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        self.assertTrue(len(image.shape) == 2)
        self.execute(image)

    def test_expand_channel(self):
        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        image = image[:, :, np.newaxis]
        self.assertTrue(len(image.shape) == 3)
        self.execute(image)

    def test_multi_channel(self):
        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        image_3ch = image[:, :, np.newaxis]
        image_3ch = np.repeat(image_3ch, 3, 2)
        self.assertTrue(len(image_3ch.shape) == 3)
        self.execute(image_3ch)

    def test_tensor_nwhc(self):
        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        image_3ch = image[:, :, np.newaxis]
        image_3ch = np.repeat(image_3ch, 3, 2)
        image_3ch = image_3ch[np.newaxis, :, :, :]
        self.assertTrue(len(image_3ch.shape) == 4)
        self.execute(image_3ch)

    def test_images_and_labels(self):
        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        image_3axis = image[:, :, np.newaxis]
        the_image = np.repeat(image_3axis, 3, 2)
        the_label = np.repeat(image_3axis, 2, 2)
        self.assertTrue(len(the_image.shape) == 3)
        self.assertTrue(len(the_label.shape) == 3)
        self.execute(the_image, the_label)


class TestContrast(unittest.TestCase, AugTest):
    def execute(self, image, labels=None):
        contrast = Contrast()
        result = process(image, contrast, labels)
        check_shape(image, result)


class TestAffine(unittest.TestCase, AugTest):
    def execute(self, image, labels=None):
        affine = Affine()
        result = process(image, affine, labels)
        # Verify that augmentation don't affect the mean value
        image_range = np.max(image) - np.min(image)
        self.assertTrue(np.abs(np.mean(result) - np.mean(image)) < image_range * 0.01)
        # Verify shapes
        check_shape(image, result)
        # Verify dynamic ranges
        self.assertTrue(np.max(image) - np.max(result) < 0.01)
        self.assertTrue(np.min(image) - np.min(result) < 0.01)


class TestElasticTransform(unittest.TestCase, AugTest):
    def execute(self, image, labels=None):
        elastic = ElasticTransform()
        result = process(image, elastic, labels)
        # Verify that augmentation don't affect the mean value
        image_range = np.max(image) - np.min(image)
        self.assertTrue(np.abs(np.mean(result) - np.mean(image)) < image_range * 0.01)
        # Verify std
        self.assertTrue(np.abs(np.std(result) - np.std(image)) < image_range * 0.01)
        # Verify shapes
        check_shape(image, result)
        # Verify dynamic ranges
        self.assertTrue(np.max(image) - np.max(result) < 0.01)
        self.assertTrue(np.min(image) - np.min(result) < 0.01)


class TestClip(unittest.TestCase, AugTest):
    def execute(self, image, labels=None):
        n_patch = 12
        size = (128, 128)
        clipper = ClipPatches(patch_size=size, n_patch=n_patch)
        result = process(image, clipper, labels)
        # Verify that augmentation don't affect the mean value
        image_range = np.max(image) - np.min(image)
        self.assertTrue(np.abs(np.mean(result) - np.mean(image)) < image_range * 0.1)
        # Verify std
        self.assertTrue(np.abs(np.std(result) - np.std(image)) < image_range * 0.1)
        # Verify shapes
        self.assertEqual(result.shape[0], n_patch)
        self.assertEqual(result.shape[1], size[0])
        self.assertEqual(result.shape[2], size[1])
        if len(image.shape) == 3:
            self.assertEqual(result.shape[3], image.shape[2])
        if len(image.shape) == 4:
            self.assertEqual(result.shape[3], image.shape[3])


class TestFlip(unittest.TestCase, AugTest):
    def execute(self, image, labels=None):
        flip_aug = Flip(force_flip=False, force_transpose=False)
        result = process(image, flip_aug, labels)
        # Verify that augmentation don't affect the mean value
        image_range = np.max(image) - np.min(image)
        self.assertTrue(np.abs(np.mean(result) - np.mean(image)) < image_range * 0.001)
        # Verify std
        self.assertTrue(np.abs(np.std(result) - np.std(image)) < image_range * 0.001)
        # Verify shapes
        check_shape(image, result)
        # Verify dynamic ranges
        self.assertTrue(np.max(image) - np.max(result) < 0.001)
        self.assertTrue(np.min(image) - np.min(result) < 0.001)


class TestCombination(unittest.TestCase):
    """
    Class for testing combination of augmentations
    """

    def test_combination(self):
        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        self.assertTrue(len(image.shape) == 2)
        augmentations = [Affine(), ClipPatches(), ElasticTransform()]  # , Contrast(image.shape)]
        # Augmentation pipeline
        prev = None
        for aug in augmentations:
            if prev is None:
                aug.prepare(image.shape)
            else:
                aug.prepare(prev.get_output_shape())
            prev = aug
            image = aug.apply(image)


if __name__ == '__main__':
    verbose = False
    unittest.main()
