import torch.utils.data as data
from PIL import Image
import random
import numpy as np
import sets
import torch

class PyTorchWrapper(data.Dataset):
    def __init__(self, ds, train=True, transform=None, target_transform=None):
        """
        :param root: the root path of the directory
        :param part: one of three part "train", "val" or "test"
        """
        assert isinstance(ds, sets.DeepBioData)
        super(PyTorchWrapper, self).__init__()
        self.transform = transform
        self.target_transform = target_transform
        self.files = False

        if train:
            self.data = ds.train
            self.labels = ds.train_labels
        else:
            self.data = ds.test
            self.labels = ds.test_labels

        if isinstance(self.data[0], basestring):
            self.files = True

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, index):
        if self.files:
            img = Image.open(self.data[index]).convert('RGB')
            target = Image.open(self.labels[index])
        else:
            img = Image.fromarray(np.uint8(self.data[index]))
            target = Image.fromarray(np.uint8(self.labels[index]))

        seed = np.random.randint(2147483647)
        random.seed(seed)
        if self.transform is not None:
            img = self.transform(img)

        random.seed(seed)
        if self.target_transform is not None:
            target = self.target_transform(target)

        target = torch.ByteTensor(np.array(target))

        return img, target