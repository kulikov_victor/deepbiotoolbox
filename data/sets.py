# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 19:08:35 2017

Module DataSet for biological data processing
Can create:
    Ecoli dataset
    BBBC dataset
    PickleDataset - load a dataset from pickle file
    domain_shift - create a copy of dataset with shifted domain (nois,blur,gamma)
Functions of classes:
    generate - sample a training batches
    save_to_pickle - saves dataset to a pickle
Function:
    plot_Batch - plot image and label    
@author: Victor Kulikov
"""
import inspect
import pickle
import threading
import os
from os import listdir
from os.path import isfile, join

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
from scipy import misc
from scipy import ndimage
from skimage import measure
from skimage.filters import sobel
from skimage.morphology import closing
from skimage.morphology import square
from skimage import morphology

from scipy.ndimage import distance_transform_edt
from scipy.ndimage.filters import sobel


class threadsafe_iter:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """

    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def next(self):
        with self.lock:
            return self.it.next()


def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """

    def g(*a, **kw):
        return threadsafe_iter(f(*a, **kw))

    return g


def normalize_label_weights(lbl):
    """
    perform normalization 1/(p+1)
    """
    if 4 == len(lbl.shape):
        x = np.zeros_like(lbl)
        for j in range(0, lbl.shape[0]):
            x[j] = normalize_label_weights(lbl[j])
        return x
    assert 3 == len(lbl.shape)
    sums = np.sum(lbl, axis=(0, 1))
    sums[sums == 0] = 1

    return np.divide(lbl, sums)


def ln_normalize_label_weights(lbl):
    """
     perform normalization 1/ln(p+1)
     """
    if 4 == len(lbl.shape):
        x = np.zeros_like(lbl)
        for j in range(0, lbl.shape[0]):
            x[j] = ln_normalize_label_weights(lbl[j])
        return x
    assert 3 == len(lbl.shape)
    sums = np.sum(lbl, axis=(0, 1))
    sums = np.log(1 + sums)
    sums[sums == 0] = 1

    return np.divide(lbl, sums)
    #


class DeepBioData:
    """Abstract class for data loading and working for used format
    Generate in format Data:[N,W,H,C=3] Labels:[N,W,H,C=3](BG,FG,EDGES)
    """

    def __init__(self):
        self.normalize = normalize_label_weights
        self.augmentation = []
        self.ndims = 1
        self.nlabels = 2
        self.flatten_labels = True


    def _normalize_image_weights(self, batch_of_images):
        """
        Perform batch normalization: subtract mean and divide by max value
        
        :param batch_of_images: batch of image NxWxHxC
        :type batch_of_images: numpy.ndarray
        :return: same size batch
        """
        if 4 == len(batch_of_images.shape):
            x = np.zeros_like(batch_of_images)
            for j in range(0, batch_of_images.shape[0]):
                x[j] = self._normalize_image_weights(batch_of_images[j])
            return x
        return (batch_of_images - self.pixel_mean) / self.max_value

    def save_pickle(self, filename):
        """
        Stores a preprocessed dataset into a pickle file
        
        Pickle file is a dictionary python object and sometimes it is much fasted to
        load preprocessed data from a pickle 
        
        :param filename: The name of file for dataset
        :return: Nothing
        """
        try:
            f = open(filename, 'wb')
            save = {
                'name': self.name,
                'ndims': self.ndims,
                'nlabels': self.nlabels,
                'train': self.train,
                'train_labels': self.train_labels,
                'classes': self.classes,
                'test': self.test,
                'test_labels': self.test_labels,
                'pixel_mean': self.pixel_mean,
                'max_value': self.max_value,
                'colors': self.colors
            }
            pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
            f.close()
            del save
        except Exception as e:
            print('Unable to save data to', filename, ':', e)
            raise

    def print_status(self):
        """
        Simple function to print inner parameters of the dataset
        
        :return: Nothing
        """
        status = {
            'name': self.name,
            'ndims': self.ndims,
            'nlabels': self.nlabels,
            'train shape': self.train.shape,
            'train_labels': self.train_labels.shape,
            'test': self.test.shape,
            'test_labels': self.test_labels.shape,
            'pixel_mean': self.pixel_mean,
            'max_value': self.max_value
        }
        print(status)

    def make_color(self, y_predicted):
        """
        The function creates a color map from labels
        
        Creates a color array using numpy.argmax function over 
        softmax output of the segmentation network
        
        :param y_pred: predicted labels
        :return: numpy.ndarray [W,H,3]
        """
        assert 3 == len(y_predicted.shape)
        x = np.argmax(y_predicted, axis=(2))
        y = np.ndarray(shape=[y_predicted.shape[0], y_predicted.shape[1], 3], dtype=np.uint8)
        for dims in range(0, y_predicted.shape[2]):
            y[x == dims, :] = self.colors[dims]
        return y

    def augmented_shape(self, image=None):
        # build augmentation pipeline
        prev = None
        if image is None:
	    if hasattr(self, 'zero_image'):
		image = self.zero_image
	    else:
            	image = self.train[0]

        for aug in self.augmentation:
            if prev is None:
                aug.prepare(image.shape)
            else:
                aug.prepare(prev.get_output_shape())
            prev = aug
        if prev is not None:
            return prev.get_output_shape()

        return 1, image.shape[0], image.shape[1], self.ndims

    def _abstract_generator(self, n_files, images, labels):
        assert images.shape[0] == labels.shape[0]

        final_shape = self.augmented_shape(images[0])

        data = np.zeros(shape=[n_files * final_shape[0], final_shape[1], final_shape[2], self.ndims], dtype=np.float32)
        if self.flatten_labels:
            classes = np.zeros(shape=[n_files * final_shape[0], final_shape[1] * final_shape[2], self.nlabels],
                               dtype=np.float32)
        else:
            classes = np.zeros(shape=[n_files * final_shape[0], final_shape[1], final_shape[2], self.nlabels],
                            dtype=np.float32)

        for f in range(0, n_files):

            sample_id = np.random.randint(0, images.shape[0])
            if len(self.augmentation) > 0:
                # If data augmentation is necessary
                image = images[sample_id]
                label = labels[sample_id]
                for aug in self.augmentation:
                    aug.prepare(image.shape)
                    image = aug.apply(image)
                    label = aug.apply(label)

                # Correct interpolation artifacts

                x = np.argmax(label, axis=(3))
                y = np.zeros_like(label)
                for dims in range(0, label.shape[3]):
                    y[x == dims, dims] = 1
                label = y

            else:
                # No data augmentation
                image = images[sample_id]
                label = labels[sample_id]

            label = self.normalize(label)

            data[f * final_shape[0]:(f + 1) * final_shape[0]] = self._normalize_image_weights(image)
            if self.flatten_labels:
                classes[f * final_shape[0]:(f + 1) * final_shape[0]] = label.reshape(final_shape[0], final_shape[1]
                                                                                 * final_shape[2], self.nlabels)
            else:
                classes[f * final_shape[0]:(f + 1) * final_shape[0]] = label

        return [data, classes]

    @threadsafe_generator
    def generate(self, n_files):
        while 1:
            [data, classes] = self._abstract_generator(n_files, self.train, self.train_labels)
            yield ({'x': data}, {'y': classes})

    @threadsafe_generator
    def generate_test(self, n_files):
        while 1:
            [data, classes] = self._abstract_generator(n_files, self.test, self.test_labels)
            yield ({'x': data}, {'y': classes})


class SkipAdapter(DeepBioData):
    def __init__(self,dataset,max_skips=2):
        assert isinstance(dataset,DeepBioData)
        self.batch_size = 1
        self.max_skips = max_skips
        for name, m in inspect.getmembers(dataset):
            if name is not "generate" and name is not "generate_test":
                setattr(self, name,m)

    @staticmethod
    def get_vector(batch_size, dims, max_skips):
        data = np.ones([batch_size, dims], dtype=np.float32)
        skips = np.random.randint(0, max_skips + 1, [batch_size])
        for i in xrange(batch_size):
            pos = np.random.randint(0, dims, [skips[i]])
            data[i][pos] = 0
        return data

    @threadsafe_generator
    def generate(self, n_files):
        shape = self.augmented_shape()
        self.batch_size = n_files * shape[0]
        while 1:
            [data, classes] = self._abstract_generator(n_files, self.test, self.test_labels)
            skips = self.get_vector(self.batch_size, self.ndims, self.max_skips)
            for index, row in enumerate(skips):
                data[index, :, :, np.where(row == 0)] = 0
            yield ({'x': data, 'c': skips}, {'y': classes})

    @threadsafe_generator
    def generate_test(self, n_files, skips=[]):
        shape = self.augmented_shape()
        self.batch_size = n_files * shape[0]
        while 1:
            [data, classes] = self._abstract_generator(n_files, self.test, self.test_labels)
            if skips is None:
                skips = self.get_vector(self.batch_size, self.ndims, self.max_skips)
            else:
                skips = np.array(skips)
                skips = skips[np.newaxis,:]
                skips = np.repeat(skips, self.batch_size, 0)

            for index, row in enumerate(np.array(skips)):
                data[index, :, :, np.where(row == 0)] = 0
            yield ({'x': data, 'c': skips}, {'y': classes})


class PickleDataset(DeepBioData):
    def __init__(self, path, augmentation=[]):
        DeepBioData.__init__(self)
        try:
            f = open(path, 'rb')
            save = pickle.load(f)
            self.name = save['name']
            self.ndims = save['ndims']
            self.nlabels = save['nlabels']
            self.train = save['train']
            self.train_labels = save['train_labels']
            self.classes = save['classes']

            self.test = save['test']
            self.test_labels = save['test_labels']
            self.pixel_mean = save['pixel_mean']
            self.max_value = save['max_value']
            self.colors = save['colors']
            f.close()
            del save
        except Exception as e:
            print('Unable to load data from', path, ':', e)
            raise e
        self.augmentation = augmentation


class BBBC(DeepBioData):
    """
    Class represents dataset from BBBC
    """

    def _custom_loss_weights(self, binary, binary_borders, gain=10., fg_weight=1., bg_weight=2.):
        binary = binary > 0.5
        binary_borders = binary_borders > 0.5
        weights = np.copy(binary).astype(np.float32)
        distance = ndimage.morphology.distance_transform_edt(binary + binary_borders)
        distance[distance > gain] = gain
        # distance = (distance / np.max(distance))*gain
        distance[distance < 1] = fg_weight  # inside cell should be fg_weight
        weights[binary] = fg_weight
        weights[(1 - binary) > 0] = bg_weight  # on background bg_weight
        weights[binary_borders] = distance[binary_borders]  # on borders between cells dist transform
        return weights / np.max(weights)

    # in the ground truth we have only contours, let's make regions from them
    def _make_binary(self, boarder_cell, nucleus, max_cell_size=7000, min_intersection=10):
        nucleus = nucleus > 0
        cell = ((1 - boarder_cell) > 0)

        nucleus = measure.label(((1 - nucleus) > 0), neighbors=4)
        # find background label it must be with biggest size
        bg_ind = np.argmax([np.sum(nucleus == l) for l in np.unique(nucleus)])
        nucleus[nucleus == bg_ind] = 0
        nucleus[nucleus != 0] = 1

        label_cell = measure.label(cell, neighbors=4)

        cells_clean = np.copy(cell)
        cells_clean.fill(0)

        for l in np.unique(label_cell):  # now we loop for each cell candidate
            if np.sum(label_cell == l) < max_cell_size:  # check if cell candidate is too big
                if (np.sum((
                                       label_cell == l) & nucleus) > min_intersection):  # the cell has a nucleus, inner region - has not
                    cells_clean[label_cell == l] = 1

        cells_clean[boarder_cell > 0] = 0
        return [cells_clean, nucleus]

    def __read_images(self, images, labels, names):
        for index, tpl in enumerate(names):
            for i in range(0, 3):
                images[index, :, :, i] = np.asarray(Image.open(tpl[i]))
            cell_border = np.asarray(misc.imread(tpl[3]), dtype=np.float32)
            nucleus_border = np.asarray(misc.imread(tpl[4]), dtype=np.float32)
            [cells, nucleus] = self._make_binary(cell_border, nucleus_border, min_intersection=15)
            labels[index, :, :, 2] = self._custom_loss_weights(cells, cell_border, fg_weight=0, bg_weight=0)
            labels[index, :, :, 1] = cells
            labels[index, :, :, 0] = np.ones_like(cells)
            labels[index, cells > 0, 0] = 0
            labels[index, labels[index, :, :, 2] > 0, 0] = 0

    def __init__(self, path, img_sz, train_percent, augmentation=[]):
        DeepBioData.__init__(self)
        self.ndims = 3
        self.nlabels = 3
        self.max_value = 255.
        self.name = "BBBC"
        self.classes = {'background': 0, 'cells': 1, 'edges': 2}
        self.colors = [(255, 0, 0), (255, 255, 0), (0, 255, 0)]
        self.augmentation = augmentation

        path_to_images = path + "normal_images/"
        path_to_labels = path + "labels/"

        # all image files
        imagesfiles = [f for f in sorted(listdir(path_to_images)) if isfile(join(path_to_images, f))]

        # all label files
        labelsfiles = [f for f in sorted(listdir(path_to_labels)) if isfile(join(path_to_labels, f))]

        # unique prefixes
        names = np.unique([x[:5] for x in imagesfiles])

        data = []
        for n in names:
            temp = [path_to_images + x for x in imagesfiles if x[:5] == n] + [path_to_labels + x for x in labelsfiles if
                                                                              x[:5] == n]
            # save only with valid DNA, pH3, activ, nuclei, cells
            if (len(temp) == 5):
                data.append(temp)

        # add random premutation
        names = np.array(data)

        n_train_images = int(len(names) * train_percent)
        n_test_images = len(names) - n_train_images

        self.train = np.ndarray(shape=[n_train_images, img_sz, img_sz, 3], dtype=np.float32)
        self.train_labels = np.ndarray(shape=[n_train_images, img_sz, img_sz, 3], dtype=np.float32)

        self.test = np.ndarray(shape=[n_test_images, img_sz, img_sz, 3], dtype=np.float32)
        self.test_labels = np.ndarray(shape=[n_test_images, img_sz, img_sz, 3], dtype=np.float32)

        # add some randomess to patches
        order = np.random.permutation(len(names))
        print('train')
        self.__read_images(self.train, self.train_labels, names[order[:n_train_images]])
        print('test')
        self.__read_images(self.test, self.test_labels, names[order[n_train_images:]])

        self.pixel_mean = self.train.mean((0, 1, 2))
        self.max_value = self.train.max()


class Drosophila(DeepBioData):
    def __readImage(self, basepath, fname):
        raw = np.asarray(Image.open(basepath + '/raw/' + fname), dtype=np.float32)
        # print(raw.shape,fname[:-3]+'png')
        mitochondria = np.asarray(misc.imread(basepath + '/mitochondria/' + fname[:-3] + 'png'), dtype=np.float32)
        # print(mitochondria.shape)
        membranes = np.asarray(misc.imread(basepath + '/membranes/' + fname[:-3] + 'png'), dtype=np.float32)
        synapses = np.asarray(misc.imread(basepath + "/synapses/" + fname[:-3] + 'png'), dtype=np.float32)
        # print(synapses.shape)
        background = np.ones_like(mitochondria)
        mask = np.logical_or(np.logical_or(mitochondria != 0, membranes != 0), synapses != 0)
        # print(mask.shape)
        background[mask] = 0
        background = background * mitochondria.max()
        # print(background.shape)
        return [raw, background, mitochondria, membranes, synapses]

    def make_color(self, y_pred):
        """
        """
        x = np.argmax(y_pred, axis=(2))
        y = np.ndarray(shape=[y_pred.shape[0], y_pred.shape[1], 3], dtype=np.uint8)
        for dims in range(0, y_pred.shape[2]):
            y[x == dims, :] = self.colors[dims]
        return y

    def __init__(self, path, img_sz, train_percent, augmentation=[]):
        DeepBioData.__init__(self)
        self.ndims = 1
        self.nlabels = 4
        self.max_value = 255.
        self.name = "Drosophila"
        self.classes = {'background': 0, 'mitochondria': 1, 'membranes': 2, 'synapses': 3}
        self.colors = [(255, 0, 0), (255, 255, 0), (0, 255, 0), (0, 0, 255)]
        self.augmentation = augmentation
        names = []
        for f in listdir(path + '/raw/'):
            try:
                if isfile(join(path + '/raw/', f) and f.endswith('.tif')):
                    names.append(f)
            except ValueError:
                pass
        print(names)
        names = np.array(names)
        n_train_images = int(len(names) * train_percent)
        n_test_images = len(names) - n_train_images

        self.train = np.ndarray(shape=[n_train_images, img_sz, img_sz, self.ndims], dtype=np.float32)
        self.train_labels = np.ndarray(shape=[n_train_images, img_sz, img_sz, self.nlabels], dtype=np.float32)

        self.test = np.ndarray(shape=[n_test_images, img_sz, img_sz, self.ndims], dtype=np.float32)
        self.test_labels = np.ndarray(shape=[n_test_images, img_sz, img_sz, self.nlabels], dtype=np.float32)

        # add some randomess to patches
        order = np.random.permutation(len(names))
        try:
            print('train')
            for index, f in enumerate(names[order[:n_train_images]]):
                [raw, background, mitochondria, membranes, synapses] = self.__readImage(path, f)
                self.train[index, :, :, 0] = raw
                self.train_labels[index, :, :, 0] = background
                self.train_labels[index, :, :, 1] = mitochondria
                self.train_labels[index, :, :, 2] = membranes
                self.train_labels[index, :, :, 3] = synapses

            print('test')
            for index, f in enumerate(names[order[n_train_images:]]):
                [raw, background, mitochondria, membranes, synapses] = self.__readImage(path, f)
                self.test[index, :, :, 0] = raw
                self.test_labels[index, :, :, 0] = background
                self.test_labels[index, :, :, 1] = mitochondria
                self.test_labels[index, :, :, 2] = membranes
                self.test_labels[index, :, :, 3] = synapses
        except ValueError as E:
            print('error', E)

        self.pixel_mean = self.train.mean((0, 1, 2))


class Ecoli(DeepBioData):
    """
    Data wrapper over Ecoli dataset
    """

    def __read_images(self, images, labels, names):
        for index, tpl in enumerate(names):
            t = np.asarray(Image.open(tpl['data']), dtype=np.float32)
            images[index] = np.repeat(t[:, :, np.newaxis], 3, axis=2)
            labels[index, :, :, 1] = np.asarray(Image.open(tpl['fg']), dtype=np.float32)
            labels[index, :, :, 2] = np.asarray(Image.open(tpl['edges']), dtype=np.float32)
            labels[index, :, :, 0] = 255 - labels[index, :, :, 1] - labels[index, :, :, 2]

    def __init__(self, path, img_sz, train_percent, augmentation=[]):
        DeepBioData.__init__(self)
        self.ndims = 3
        self.nlabels = 3
        self.max_value = 255.
        self.name = "Ecoli"
        self.classes = {'background': 0, 'objects': 1, 'edges': 2}
        self.colors = [(255, 0, 0), (255, 255, 0), (0, 255, 0)]
        self.augmentation = augmentation
        names = []

        for f in listdir(path):
            try:
                if (isfile(join(path, f)) and f.endswith('.png')):
                    x = int(f[:-4])
                    names.append({'data': join(path, f), 'fg': join(path, str(x) + '_fg.png'),
                                  'edges': join(path, str(x) + '_edges.png')})

            except ValueError:
                pass
        names = np.array(names)

        n_train_images = int(len(names) * train_percent)
        n_test_images = len(names) - n_train_images

        self.train = np.ndarray(shape=[n_train_images, img_sz, img_sz, 3], dtype=np.float32)
        self.train_labels = np.ndarray(shape=[n_train_images, img_sz, img_sz, 3], dtype=np.float32)

        self.test = np.ndarray(shape=[n_test_images, img_sz, img_sz, 3], dtype=np.float32)
        self.test_labels = np.ndarray(shape=[n_test_images, img_sz, img_sz, 3], dtype=np.float32)

        # add some randomess to patches
        order = np.random.permutation(len(names))
        print('train')
        self.__read_images(self.train, self.train_labels, names[order[:n_train_images]])
        print('test')
        self.__read_images(self.test, self.test_labels, names[order[n_train_images:]])

        self.pixel_mean = self.train.mean((0, 1, 2))


class Hoechst(DeepBioData):
    """
    Data wraper over Hoechst dataset
    """

    def __readLabel(self, path):
        ydir = join(self.biodatadir, 'y')
        img = np.asarray(misc.imread(join(ydir, path) + '.png'), dtype=np.float32)
        labels = np.zeros([img.shape[0], img.shape[1], 3], dtype=np.float32)
        img = img / np.max(img)
        s = sobel(img)
        s[s > 0] = 1

        binimg = img
        binimg[img > 0] = 1
        binimg[s > 0] = 0
        closed = closing(binimg, square(5))
        labels[:, :, 2] = closed - binimg
        labels[:, :, 0] = 1 - closed
        labels[:, :, 1] = binimg
        return labels

    def __readData(self, name):
        img_id = self.data[self.data['channel_id'] == name]['image_id']

        print(img_id.values)
        files = self.data[self.data['image_id'] == img_id.values[0]]['channel_id']
        xdir = join(self.biodatadir, 'x')

        np_tensor = np.zeros([self.W, self.H, self.ndims], dtype=np.float32)
        files = np.array(files)
        for i in files:
            # print(self.data[self.data['channel_id']==i]['stain_description'].values)
            stain = self.data[self.data['channel_id'] == i]['stain_description'].values[0]
            np_tensor[:, :, self.data_order[stain]] = np.asarray(misc.imread(join(xdir, i) + '.png'), dtype=np.float32)
        return np_tensor

    def __init__(self, train_percent, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations', augmentation=[],use_random=False):
        DeepBioData.__init__(self)
        self.biodatadir = path
        self.ndims = 5
        self.nlabels = 3
        self.max_value = 255.
        self.name = "Hoechst"
        self.classes = {'background': 0, 'objects': 1, 'edges': 2}
        self.colors = [(255, 0, 0), (255, 255, 0), (0, 255, 0), (0, 0, 255),(128,128,0),(255,22,10)]
        self.augmentation = augmentation

        # Get all labels
        self.data = pd.read_csv(join(path, 'metadata.csv'))
        labelsdir = join(path, 'y')
        y_imagesfiles = [f[:-4] for f in sorted(listdir(labelsdir)) if
                         isfile(join(labelsdir, f)) and f.endswith('.png')]

        y_imagesfiles = np.array(y_imagesfiles)

        test_y = self.__readLabel(y_imagesfiles[0])
        self.W = test_y.shape[0]
        self.H = test_y.shape[1]

        img_id = self.data[self.data['channel_id'] == y_imagesfiles[0]]['image_id']

        files = self.data[self.data['image_id'] == img_id.values[0]]['channel_id']
        self.data_order = {}
        for index, i in enumerate(files):
            self.data_order[self.data[self.data['channel_id'] == i]['stain_description'].values[0]] = index

        n_train_images = int(len(y_imagesfiles) * train_percent)
        n_test_images = len(y_imagesfiles) - n_train_images

        self.train = np.ndarray(shape=[n_train_images, self.W, self.H, self.ndims], dtype=np.float32)
        self.train_labels = np.ndarray(shape=[n_train_images, self.W, self.H, self.nlabels], dtype=np.float32)

        self.test = np.ndarray(shape=[n_test_images, self.W, self.H, self.ndims], dtype=np.float32)
        self.test_labels = np.ndarray(shape=[n_test_images, self.W, self.H, self.nlabels], dtype=np.float32)


        order = range(len(y_imagesfiles))
        if use_random:
            # add some randomness to patches
            order = np.random.permutation(len(y_imagesfiles))

        print('train')
        for index, filename in enumerate(y_imagesfiles[order[:n_train_images]]):
            self.train[index] = self.__readData(filename)
            self.train_labels[index] = self.__readLabel(filename)

        print('test')
        for index, filename in enumerate(y_imagesfiles[order[n_train_images:]]):
            self.test[index] = self.__readData(filename)
            self.test_labels[index] = self.__readLabel(filename)

        mv = self.train.max((0, 1, 2))
        self.train = self.train / mv
        self.test = self.test / mv

        self.pixel_mean = self.train.mean((0, 1, 2))
        self.max_value = self.train.max()


class Warwick(DeepBioData):
    """
    Warwick dataset from GlaS challenge MICAI2015
    """
    @staticmethod
    def _get_images_names(path, pattern):
        return [os.path.join(path, item) for item in os.listdir(os.path.join(path))
                if os.path.isfile(os.path.join(path, item)) and item.endswith(".bmp") and (pattern in item)]

    def __init__(self, rootpath, augmentation=[], contours=None, contour_width=12,scale =1):
        self.name = 'Warwick'
        DeepBioData.__init__(self)
        self.normalize = ln_normalize_label_weights
        self.augmentation = augmentation
        self.ndims = 3
        self.nlabels = 2
        self.max_value = 255.
        self.pixel_mean = 128.
        self.contours = contours
        self.scale = scale
        self.contour_width = contour_width
        self.classes = {"backgound": 0, "object": 1}

        self.angles_step=np.pi/2
        if contours == "angles":
            self.nlabels = int(2*np.pi/self.angles_step +1)

        self.colors = [(255, 0, 0), (0, 0, 255)]

        train_files = self._get_images_names(rootpath, "train")
        data_files = [f for f in train_files if "anno" not in f]
        self.train = np.array(data_files)
        self.train_labels = np.array([f[:-4] + "_anno.bmp" for f in data_files])

        test_files = self._get_images_names(rootpath, "test")
        data_files = [f for f in test_files if "anno" not in f]
        self.test = np.array(data_files)
        self.test_labels = np.array([f[:-4] + "_anno.bmp" for f in data_files])


        self.zero_image = self._load_image(self.train[0])

        self.rootpath = rootpath

    def save_pickle(self, filename):
        raise "Not supported"

    def _load_image(self, name):
        pil_image = Image.open(name)
        if 1 != self.scale:
            pil_image = pil_image.resize([int(self.scale * s) for s in pil_image.size])
        return np.array(pil_image).astype(np.float32)

    @staticmethod
    def prepare_dataset(source_path,dest_path):
        files = [os.path.join(source_path, item) for item in os.listdir(source_path)
                 if os.path.isfile(os.path.join(source_path, item)) and item.endswith(".bmp")]

        minfile = np.argmin(np.array([os.stat(f).st_size for f in files]))
        I = Image.open(files[minfile])
        shape = np.array(I).shape

        for f in files:
            I = Image.open(f)
            I2 = I.crop((0, 0, shape[1], shape[0]))
            fx = f.replace(source_path, dest_path)
            I2.save(fx)

    @staticmethod
    def labels_to_onehot(label, numlabels):
        label = label.astype(np.int32)
        onehot = np.zeros((label.shape[0], label.shape[1], label.shape[2], numlabels), dtype=np.float32)
        for j in range(label.shape[0]):
            for i in range(numlabels):
                onehot[j, label[j, :, :, 0] == i, i] = 1
        return onehot

    @staticmethod
    def angle_class(img, step=np.pi / 2):
        distance = distance_transform_edt(img)
        dx = sobel(distance, axis=0)
        dy = sobel(distance, axis=1)

        norm = np.sqrt(dx ** 2 + dy ** 2)
        dx[norm != 0] = dx[norm != 0] / norm[norm != 0]
        dy[norm != 0] = dy[norm != 0] / norm[norm != 0]

        dirs = np.zeros_like(norm)
        dirs[norm != 0] = np.arctan2(dy[norm != 0], dx[norm != 0]) + np.pi-0.00000001

        x = np.round(dirs / step)
        x[x == 0] = np.max(x)
        x[norm == 0] = 0
        return x.astype(dtype=np.int32)

    def _abstract_generator(self, n_files, images, labels):
        assert images.shape[0] == labels.shape[0]

        final_shape = self.augmented_shape(self.zero_image)
        data = np.zeros(shape=[n_files * final_shape[0], final_shape[1], final_shape[2], self.ndims], dtype=np.float32)
        classes = np.zeros(shape=[n_files * final_shape[0], final_shape[1] * final_shape[2], self.nlabels],
                           dtype=np.float32)

        for f in range(0, n_files):

            sample_id = np.random.randint(0, images.shape[0])
            image = self._load_image(images[sample_id])
            annot = self._load_image(labels[sample_id])
            label = np.zeros_like(annot)
            label[np.array(annot) > 0] = 1

            if self.contours == "angles":
                label = self.angle_class(label,self.angles_step)

            if self.contours == "contours":
                edges = morphology.dilation(label, morphology.disk(1)) - label
                label = morphology.dilation(edges, morphology.disk(self.contour_width))

            if len(self.augmentation) > 0:
                # If data augmentation is necessary
                for aug in self.augmentation:
                    aug.prepare(image.shape)
                    image = aug.apply(image)
                    label = aug.apply(label)

            else:
                label = label[np.newaxis, :, :, np.newaxis]

            label = self.labels_to_onehot(np.rint(label), self.nlabels)
            # Correct interpolation artifacts

            label = np.round(label)
            label = self.normalize(label)

            data[f * final_shape[0]:(f + 1) * final_shape[0]] = self._normalize_image_weights(image)
            classes[f * final_shape[0]:(f + 1) * final_shape[0]] = label.reshape(final_shape[0], final_shape[1]
                                                                                 * final_shape[2], self.nlabels)
        return [data, classes]


def plot_batch(X, y, index, gain_image=255, gain_label=100):
    a = X['x'][index] - np.min(X['x'][index])
    if np.max(a) > 0:
        a = a / np.max(a)
    b = y['y'][index] / np.max(y['y'][index])

    l = int(np.sqrt(b.shape[0]))
    b = b.reshape(l, l, b.shape[1])
    f, axarr = plt.subplots(1, 2, figsize=(10, 10))
    axarr[0].imshow(a * gain_image, cmap='gray')
    axarr[0].set_title('Image')
    axarr[1].imshow(b * gain_label, cmap='gray')
    axarr[1].set_title('Labels')
