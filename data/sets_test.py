import time
import unittest

import matplotlib.pyplot as plt
from PIL import Image

from augmentation import *
from sets import DeepBioData, SkipAdapter


class DebugSet(DeepBioData):
    """
    Fake class for testing DeepBioData superclass functions
    """

    def __init__(self, augmentation=[]):
        DeepBioData.__init__(self)
        self.ndims = 3
        self.nlabels = 3
        self.max_value = 255.
        self.name = "DebugSet"
        self.classes = {'background': 0, 'cells': 1, 'edges': 2}
        self.colors = [(255, 0, 0), (255, 255, 0), (0, 255, 0)]
        self.augmentation = augmentation

        n_train_images = 10
        n_test_images = 5
        img_sz = 128

        image = np.asarray(Image.open("../test_images/ecoli.png"), dtype=np.float)
        edges = np.asarray(Image.open("../test_images/ecoli_edges.png"), dtype=np.float)
        foreground = np.asarray(Image.open("../test_images/ecoli_fg.png"), dtype=np.float)
        background = foreground.max() - foreground - edges
        img_sz = image.shape[0]

        self.train = np.ndarray(shape=[n_train_images, img_sz, img_sz, 1], dtype=np.float32)
        self.train_labels = np.ndarray(shape=[n_train_images, img_sz, img_sz, 3], dtype=np.float32)

        for i in xrange(n_train_images):
            self.train[i, :, :, 0] = image
            self.train_labels[i, :, :, 0] = background
            self.train_labels[i, :, :, 1] = foreground
            self.train_labels[i, :, :, 2] = edges

        self.test = np.ndarray(shape=[n_test_images, img_sz, img_sz, 1], dtype=np.float32)
        self.test_labels = np.ndarray(shape=[n_test_images, img_sz, img_sz, 3], dtype=np.float32)

        for i in xrange(n_test_images):
            self.test[i, :, :, 0] = image
            self.test_labels[i, :, :, 0] = background
            self.test_labels[i, :, :, 1] = foreground
            self.test_labels[i, :, :, 2] = edges

        self.pixel_mean = self.train.mean((0, 1, 2))
        self.max_value = self.train.max()


class TestDeepBioData(unittest.TestCase):
    def create_dataset(self, augmentation=[]):
        return DebugSet(augmentation)

    def test_creation(self):
        dataset = self.create_dataset()

    def test_generator(self):
        dataset = self.create_dataset()
        gen = dataset._abstract_generator(5, dataset.train, dataset.train_labels)
        self.assertEqual(gen[0].shape[0], 5)
        if (verbose):
            plt.figure(figsize=(10, 10))
            plt.imshow(gen[0][0, :, :, 0], cmap='gray')
            b = gen[1][0]
            l = int(np.sqrt(b.shape[0]))
            b = b.reshape(l, l, b.shape[1])
            plt.figure(figsize=(10, 10))
            plt.imshow(b * 100, cmap='gray')

    def test_augmentation(self):
        augmentations = [ClipPatches(), Affine(), ElasticTransform()]
        dataset = self.create_dataset(augmentation=augmentations)

        beg_ts = time.time()
        gen = dataset._abstract_generator(5, dataset.train, dataset.train_labels)
        end_ts = time.time()
        print("elapsed time: %f" % (end_ts - beg_ts))
        # self.assertEqual(gen[0].shape[0], 5)
        if (verbose):
            plt.figure(figsize=(10, 10))
            plt.imshow(gen[0][0, :, :, 0], cmap='gray')
            b = gen[1][0]
            l = int(np.sqrt(b.shape[0]))
            b = b.reshape(l, l, b.shape[1])
            plt.figure(figsize=(10, 10))
            plt.imshow(b * 100, cmap='gray')


"""
class TestDatasets(unittest.TestCase):
    def test_BBBC(self):
        raise "Not implemented"

    def test_Ecoli(self):
        raise "Not implemented"

    def test_Hoechst(self):
        raise "Not implemented"

    def test_Drosophila(self):
        raise "Not implemented"
"""


class TestSkipAdapter(unittest.TestCase):
    def test_creation(self):
        source_set = DebugSet()
        skip_adapter = SkipAdapter(source_set)
        self.assertEqual(source_set.test.shape, skip_adapter.test.shape)
        self.assertEqual(source_set.train.shape, skip_adapter.train.shape)
        self.assertEqual(source_set.test_labels.shape, skip_adapter.test_labels.shape)
        self.assertEqual(source_set.train_labels.shape, skip_adapter.train_labels.shape)

    def test_skips(self):
        source_set = DebugSet()
        skip_adapter = SkipAdapter(source_set)
        gen = skip_adapter.generate(10)
        [x, y] = gen.next()
        assert 'c' in x
        self.assertEqual(x['x'].shape[0], x['c'].shape[0])
        self.assertEqual(x['c'].shape[1], y['y'].shape[2])

    def test_manual_skip(self):
        source_set = DebugSet()
        skip_adapter = SkipAdapter(source_set, max_skips=1)
        gen = skip_adapter.generate(10)
        [x, y] = gen.next()
        gen_test = skip_adapter.generate_test(10, [1, 1, 0])
        [xt, yt] = gen_test.next()
        self.assertEqual(x['c'].shape, xt['c'].shape)
        self.assertEqual(x['c'].shape[1], y['y'].shape[2])


if __name__ == '__main__':
    verbose = True
    unittest.main()
