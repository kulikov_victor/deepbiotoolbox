from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
# Imports
import resource
import sys

import numpy as np
from PIL import Image

sys.path.insert(0, '/home/vkulikov/Scripts/Cityscapes')
from data import sets
from data.augmentation import ClipPatches
from segmentation import networks
from cityscapesscripts.helpers import labels as cityscapeslabels
from keras.callbacks import Callback
from segmentation.custom_layers import GaussianSmoothConv


class MemoryCallback(Callback):
    def on_epoch_end(self, epoch, log={}):
        print(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)


class Cityscapes(sets.DeepBioData):
    @staticmethod
    def _get_images_names(path):
        dirlist = [item for item in os.listdir(path) if os.path.isdir(os.path.join(path, item))]
        files = []
        for directory in dirlist:
            files += [os.path.join(path, directory, item) for item in os.listdir(os.path.join(path, directory)) if
                      os.path.isfile(os.path.join(path, directory, item)) and item.endswith(".png")]
        return files

    def __init__(self, rootpath, augmentation=[], scale=1.):
        self.name = 'Cityscapes'
        sets.DeepBioData.__init__(self)
        self.normalize = sets.ln_normalize_label_weights
        self.augmentation = augmentation
        self.ndims = 3
        self.nlabels = 34
        self.max_value = 255.
        self.pixel_mean = 128.
        self.scale = scale
        self.classes = {}
        [self.classes.update({l.name: l.id}) for l in cityscapeslabels.labels]
        self.colors = [l.color for l in cityscapeslabels.labels]

        self.train = self._get_images_names(rootpath + "leftImg8bit/train")
        self.train_labels = self._get_images_names(rootpath + "gtFine/train")
        self.train_labels = np.array([f for f in self.train_labels if f.endswith("_gtFine_labelIds.png")])

        self.test = self._get_images_names(rootpath + "leftImg8bit/val")
        self.test_labels = self._get_images_names(rootpath + "gtFine/val")
        self.test_labels = np.array([f for f in self.test_labels if f.endswith("_gtFine_labelIds.png")])

        self.train = np.sort(np.array(self.train))
        self.train_labels = np.sort(np.array(self.train_labels))
        self.test = np.sort(np.array(self.test))
        self.test_labels = np.sort(np.array(self.test_labels))

        self.zero_image = self._load_image(self.train[0])

        self.rootpath = rootpath

    def save_pickle(self, filename):
        raise "Not supported"

    @staticmethod
    def labels_to_onehot(label, numlabels):
        label = label.astype(np.int32)
        onehot = np.zeros((label.shape[0], label.shape[1], label.shape[2], numlabels), dtype=np.float32)
        for j in range(label.shape[0]):
            for i in range(numlabels):
                onehot[j, label[j, :, :, 0] == i, i] = 1
        return onehot

    def _load_image(self, name):
        pil_image = Image.open(name)
        if 1 != self.scale:
            pil_image = pil_image.resize([int(self.scale * s) for s in pil_image.size])
        return np.array(pil_image).astype(np.float32)

    def _abstract_generator(self, n_files, images, labels):
        assert images.shape[0] == labels.shape[0]

        final_shape = self.augmented_shape(self.zero_image)
        data = np.zeros(shape=[n_files * final_shape[0], final_shape[1], final_shape[2], self.ndims], dtype=np.float32)
        classes = np.zeros(shape=[n_files * final_shape[0], final_shape[1] * final_shape[2], self.nlabels],
                           dtype=np.float32)

        for f in range(0, n_files):

            sample_id = np.random.randint(0, images.shape[0])
            image = self._load_image(images[sample_id])
            label = self._load_image(labels[sample_id])

            if len(self.augmentation) > 0:
                # If data augmentation is necessary
                for aug in self.augmentation:
                    aug.prepare(image.shape)
                    image = aug.apply(image)
                    label = aug.apply(label)
            else:
                label = label[np.newaxis, :, :, np.newaxis]

            # Correct interpolation artifacts
            label = self.labels_to_onehot(np.rint(label), self.nlabels)
            label = self.normalize(label)

            data[f * final_shape[0]:(f + 1) * final_shape[0]] = self._normalize_image_weights(image)
            classes[f * final_shape[0]:(f + 1) * final_shape[0]] = label.reshape(final_shape[0], final_shape[1]
                                                                                 * final_shape[2], self.nlabels)
        return [data, classes]


aug = [ClipPatches(patch_size=(128, 128), n_patch=5)]
cityscapes = Cityscapes("/media/hpc-4_Raid/vkulikov/Cityscapes/Small/", aug, scale=0.2)
net = networks.SmoothIncResN(cityscapes, weights_file='../models/cityscapes_lowpass_gauss', output_maps=16, layer_number=4,verbose=True,smooth_class=GaussianSmoothConv)
net.train(1, augmentations=aug, steps_per_epoch=500, epochs=40, verbose=2, workers=6, callbacks=[MemoryCallback()])
