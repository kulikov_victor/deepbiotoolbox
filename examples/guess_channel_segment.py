from data.augmentation import ElasticTransform, Affine, ClipPatches
from data.sets import Hoechst,SkipAdapter
from segmentation.networks import AdaptedIncN
from classification.networks import ClassifyChannels

import numpy as np
import matplotlib.pyplot as plt
# WARNING!!!
# Before running this example you should run
# 1. train_classify_hoechst_channels
# 2. train_hoechst_skips_model


# This example demonstrates the training procedure using deepbiotoolbox with channel skips in just few lines of code
# Step 1.Lets define dataset we will use
# path defines the directory were is metadata.csv
dataset = SkipAdapter(Hoechst(0.9, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations'),max_skips=2)
# after single load we can save the dataset as a pickle for faster access in the future:


# 3. Creation of the adapted network, weights_file - defines the path of weights storage file
class_net = ClassifyChannels(dataset,weights_file="../models/Class_hoechst")
seg_net = AdaptedIncN(dataset, weights_file="../models/Inc_hoechst_skips_adapted")
# 4. Set our image
image = dataset.test[0][:,:,3]
# 5. Predict the optimal channel for that image
c_pred = np.array(class_net.process_image(image))
pos = np.argmax(c_pred)
print(c_pred,pos)
# 6. Build image in convenient form
x = np.zeros([image.shape[0],image.shape[1],dataset.ndims],dtype=np.float32)
x[:,:,pos] = image
# 7. clean c_pred
c_pred.fill(0)
c_pred[pos]=1
c_pred  = c_pred.reshape([-1,5])

color = seg_net.process_image(x,c_pred)
# 8. Show the results
f, axarr = plt.subplots(1, 2,figsize=(20, 10))

# Convert label from 2d to 3d
ground_truth = dataset.test_labels[0].reshape(dataset.test.shape[1], dataset.test.shape[2], dataset.nlabels)
axarr[0].imshow(dataset.make_color(ground_truth))
axarr[0].set_title('Ground truth')
axarr[1].imshow(color)
axarr[1].set_title('Segmentation result')
plt.waitforbuttonpress()


