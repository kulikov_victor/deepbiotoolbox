from data.sets import Hoechst
from segmentation.networks import IncN
from classification.networks import ClassifyChannels
import matplotlib.pyplot as plt

# This example demonstrates the using of deepbiotoolbox in just few lines of code
# Step 1.Lets define dataset we will use
# path defines the directory were is metadata.csv
dataset = Hoechst(0.9, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations')
# 2. Creation of the network, weights_file - defines the path of the pretrained weights storage file
class_net = ClassifyChannels(dataset,weights_file="../models/Class_hoechst")

print(class_net.test(n_test=10,ax=1)[4])
