from data.augmentation import ElasticTransform, Flip, ClipPatches
from data.sets import Hoechst
from classification.networks import ClassifyChannels

# This example demonstrates the training procedure using deepbiotoolbox in just few lines of code
# Step 1.Lets define dataset we will use
# path defines the directory were is metadata.csv
dataset = Hoechst(0.9, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations')
# after single load we can save the dataset as a pickle for faster access in the future:
# dataset.save_pickle("Hoechst.pickle")
# Step 2. Lets define the augmentation we want to use on our dataset
augmentation = [ClipPatches(n_patch=8,patch_size=(256,256)), ElasticTransform(), Flip()]

# 3. Creation of the network, weights_file - defines the path of weights storage file
net = ClassifyChannels(dataset, weights_file="../models/Class_hoechst")
# 4. Run the training (on TitanX GPU ~20 minutes) should get about 91% accuracy
net.train(n_files=2, augmentations=augmentation, steps_per_epoch=200, epochs=10)
print(net.test(n_test=100,ax=1)[4])
