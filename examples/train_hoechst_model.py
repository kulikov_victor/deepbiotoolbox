from data.augmentation import ElasticTransform, Affine, ClipPatches
from data.sets import Hoechst,ln_normalize_label_weights
from segmentation.networks import IncN,IncResN,SmoothIncResN
import matplotlib.pyplot as plt

# This example demonstrates the training procedure using deepbiotoolbox in just few lines of code
# Step 1.Lets define dataset we will use
# path defines the directory were is metadata.csv
dataset = Hoechst(0.9, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations')
#dataset.normalize = ln_normalize_label_weights
# after single load we can save the dataset as a pickle for faster access in the future:
# dataset.save_pickle("Hoechst.pickle")
# Step 2. Lets define the augmentation we want to use on our dataset
augmentation = [Affine(), ClipPatches(n_patch=18), ElasticTransform()]

net = IncResN(dataset, weights_file="../models/SIncResN_hoechst2",verbose=True)
# 3. Creation of the network, weights_file - defines the path of weights storage file
net = SmoothIncResN(dataset, weights_file="../models/SmoothIncResN_hoechst2",verbose=True)
# 4. Run the training (on TitanX GPU ~14 minutes) should get about 95% accuracy
net.train(n_files=2, augmentations=augmentation, steps_per_epoch=100, epochs=10)
print(net.test())

# 3. Run processing
color = net.process_image(dataset.test[0])
# 4. Show the results
f, axarr = plt.subplots(1, 2,figsize=(20, 10))

# Convert label from 2d to 3d
ground_truth = dataset.test_labels[0].reshape(dataset.test.shape[1], dataset.test.shape[2], dataset.nlabels)
axarr[0].imshow(dataset.make_color(ground_truth))
axarr[0].set_title('Ground truth')
axarr[1].imshow(color)
axarr[1].set_title('Segmentation result')
plt.waitforbuttonpress()