from data.augmentation import ElasticTransform, Affine, ClipPatches
from data.sets import Hoechst,SkipAdapter, ln_normalize_label_weights
from segmentation.networks import AdaptedIncN,AdaptedResIncN

# This example demonstrates the training procedure using deepbiotoolbox with channel skips in just few lines of code
# Step 1.Lets define dataset we will use
# path defines the directory were is metadata.csv
dataset = SkipAdapter(Hoechst(0.9, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations'),max_skips=2)
dataset.normalize = ln_normalize_label_weights
# after single load we can save the dataset as a pickle for faster access in the future:
# dataset.save_pickle("Hoechst.pickle")
# Step 2. Lets define the augmentation we want to use on our dataset
augmentation = [Affine(), ClipPatches(n_patch=18), ElasticTransform()]

# 3. Creation of the adapted network, weights_file - defines the path of weights storage file
net = AdaptedIncN(dataset, weights_file="../models/Inc_hoechst_skips_adapted",n_adapter_layers=3)
# 4. Run the training (on TitanX GPU ~14 minutes) should get about 95% accuracy
net.train(n_files=2, augmentations=augmentation, steps_per_epoch=100, epochs=10)