from data.sets import Hoechst
from segmentation.networks import IncN
import matplotlib.pyplot as plt

# This example demonstrates the using of deepbiotoolbox in just few lines of code
# Step 1.Lets define dataset we will use
# path defines the directory were is metadata.csv
dataset = Hoechst(0.9, path='/media/hpc-4_Raid/vkulikov/MProject/Bio/annotations')
# 2. Creation of the network, weights_file - defines the path of the pretrained weights storage file
net = IncN(dataset, weights_file="../models/Inc_hoechst")

# 3. Run processing
color = net.process_image(dataset.test[0])
# 4. Show the results
f, axarr = plt.subplots(1, 2,figsize=(20, 10))

# Convert label from 2d to 3d
ground_truth = dataset.test_labels[0].reshape(dataset.test.shape[1], dataset.test.shape[2], dataset.nlabels)
axarr[0].imshow(dataset.make_color(ground_truth))
axarr[0].set_title('Ground truth')
axarr[1].imshow(color)
axarr[1].set_title('Segmentation result')
plt.waitforbuttonpress()
