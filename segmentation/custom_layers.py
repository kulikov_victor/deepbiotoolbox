from __future__ import division
import tensorflow as tf
from keras.engine.topology import Layer
from skimage.filter import gaussian_filter
import numpy as np


class PairwiseConv(Layer):
    """
    Class for pairwise convolution of two arrays
    """

    def __init__(self, split_number, **kwargs):
        """
        
        :param split_number: size of the batch
        :param kwargs: 
        """
        self.split_number = split_number
        self.is_placeholder = False
        super(PairwiseConv, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        super(PairwiseConv, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        batches = tf.split(x[0], self.split_number)
        filters = tf.split(x[1], self.split_number)
        p = []
        for i in xrange(self.split_number):
            p.append(tf.nn.conv2d(batches[i], filters[i], strides=[1, 1, 1, 1], padding='SAME'))

        return tf.concat(p, axis=0)

    def compute_output_shape(self, input_shape):
        output_shape = [(input_shape[0][0],input_shape[0][1],input_shape[0][2],input_shape[1][3])]
        return output_shape

"""
class SmoothConv(Layer):
    def __init__(self,split_number, kernel, **kwargs):
        self.split_number = split_number
        self.is_placeholder = False
        self.kernel = kernel
        self.smooth_filter = tf.constant(1. / (self.kernel ** 2), dtype=tf.float32, shape=(self.kernel, self.kernel))
        super(SmoothConv, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        super(SmoothConv, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        print (x.shape)
        batches = tf.split(x, self.split_number )
        p = []
        for i in xrange(self.split_number ):
            print (batches[i].shape)
            channels = tf.split(batches[i], x.shape[3],axis=2)
            t = []
            for j in xrange(x.shape[3]):
                t.append(tf.nn.conv2d(channels[j], self.smooth_filter, strides=[1, 1, 1, 1], padding='SAME'))
            p.append(tf.concat(t, axis=2))
        return tf.concat(p, axis=0)

    def compute_output_shape(self, input_shape):
        return input_shape
"""


class SmoothConv(Layer):
    """
    Smooth each channel separatly
    """
    def __init__(self,kernel, **kwargs):
        """
        
        :param kernel: Size of smoothing filter
        :param kwargs: 
        """
        self.is_placeholder = False
        self.kernel = kernel
        self.smooth_filter = tf.constant(1. / (self.kernel ** 2), dtype=tf.float32, shape=(self.kernel, self.kernel,1,1))
        super(SmoothConv, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        super(SmoothConv, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        p=[]
        for i in xrange(x.shape[3]):
            p.append(tf.nn.conv2d(x[:,:,:,i:i+1], self.smooth_filter, strides=[1, 1, 1, 1], padding='SAME'))
        return tf.concat(p, axis=3)

    def compute_output_shape(self, input_shape):
        return input_shape


class GaussianSmoothConv(SmoothConv):
    """
    Smooth each channel separatly with gaussian kernel
    """

    def __init__(self, kernel, **kwargs):
        """

        :param kernel: Size of smoothing filter
        :param sigma: Variation of gaussian
        :param kwargs: 
        """

        super(GaussianSmoothConv, self).__init__(kernel,**kwargs)

        sigma = self.kernel/2.
        gauss_filter = np.zeros((self.kernel, self.kernel))
        gauss_filter[int((self.kernel-1)/2),int((self.kernel-1)/2)] = 1
        gauss_filter = gaussian_filter(gauss_filter,sigma)
        gauss_filter /= np.sum(gauss_filter)

        self.smooth_filter = tf.constant(gauss_filter, dtype=tf.float32,
                                         shape=(self.kernel, self.kernel, 1, 1))
