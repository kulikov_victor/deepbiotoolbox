"""
Metrics to measure segmentation performance

Victor Kulikov
Skoltech 2017
"""
import numpy as np


def true_positive(y, y_pred, label, ax=(2)):
    """
    Estimates the true positive rate

    :param y: ground truth labels
    :type y: numpy.ndarray
    :param y_pred: predicted labels
    :type y_pred: numpy.ndarray
    :param label: a label for which the true positive rate should be estimated
    :type label: int

    :returns: the mean correct predicted labels

    """
    return np.sum(np.logical_and(np.argmax(y, axis=ax) == label, np.argmax(y_pred, axis=ax) == label))


def true_negative(y, y_pred, label, ax=(2)):
    return np.sum(np.logical_and(np.argmax(y, axis=ax) != label, np.argmax(y_pred, axis=ax) != label))


def false_negative(y, y_pred, label, ax=(2)):
    return np.sum(np.logical_and(np.argmax(y, axis=ax) == label, np.argmax(y_pred, axis=ax) != label))


def false_positive(y, y_pred, label, ax=(2)):
    return np.sum(np.logical_and(np.argmax(y, axis=ax) != label, np.argmax(y_pred, axis=ax) == label))


def accuracy(y, y_pred, ax=(2)):
    """
    Returns the mean correct predicted labels 
    
    :param Y: ground truth labels
    :type Y: numpy.ndarray
    :param Y_pred: predicted labels
    :type Y_pred: numpy.ndarray
    
    :returns: the mean correct predicted labels
    """
    denominator = 1.0
    for i in xrange(ax):
        denominator *= y.shape[i]

    yy = np.argmax(y, axis=ax)
    bb = np.argmax(y_pred, axis=ax)
    s = np.sum(yy == bb) / float(denominator)
    return s


def recall(y, y_pred, label, ax=(2)):
    """
    Estimates the recall for a given label
    
    :param y: ground truth label
    :type y: numpy.ndarray
    :param y_pred: predicted label
    :type y_pred: numpy.ndarray
    :param label: predicted label
    :type label: int
    
    :returns: the recall for a given label
    """
    tp = true_positive(y, y_pred, label, ax)
    fn = false_negative(y, y_pred, label, ax)
    if 0 == tp + fn:
        fn = 1
    return tp / float(tp + fn)


def precision(y, y_pred, label, ax=(2)):
    """
    Estimates the precision for a given label
    
    :param y: ground truth label
    :type y: numpy.ndarray
    :param y_pred: predicted label
    :type y_pred: numpy.ndarray
    :param label: predicted label
    :type label: int
    
    :returns: the precision for a given label
    """
    fp = true_positive(y, y_pred, label, ax)
    tp = false_positive(y, y_pred, label, ax)
    if 0 == tp + fp:
        fp = 1
    return fp / float(tp + fp)


def IoU(y, y_pred, label, ax=(2)):
    """
    Returns the intersection over union for a given label
    
    :param y: ground truth label, 
    :param y_pred: predicted label
    :param label: specifies the label to estimate
    :returns
    """
    tp = true_positive(y, y_pred, label, ax)
    fp = false_positive(y, y_pred, label, ax)
    fn = false_negative(y, y_pred, label, ax)
    if 0 == tp + fn + fp:
        fn = 1
    return tp / float(tp + fn + fp)


def mean_over_labels_func(func, y, y_pred, ax=(2)):
    value = 0
    n_labels = y.shape[ax]

    for l in range(0, n_labels):
        value += func(y, y_pred, l, ax)
    return value / n_labels


def mean_recall(y, y_pred, ax=(2)):
    return mean_over_labels_func(recall, y, y_pred, ax)


def mean_precision(y, y_pred, ax=(2)):
    return mean_over_labels_func(precision, y, y_pred, ax)


def mean_IoU(y, y_pred, ax=(2)):
    return mean_over_labels_func(IoU, y, y_pred, ax)
