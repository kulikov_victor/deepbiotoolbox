import unittest
import numpy as np
from metrics import *

class TestMetrics:
    def __init__(self):
        pass

    def create_data_match(self):
        raise "Not implemented"

    def create_data_not_match(self):
        raise "Not implemented"

    def create_data_half_match(self):
        raise "Not implemented"

    def test_accuracy(self):
        [a,b,ax] = self.create_data_match()
        self.assertEqual(1,accuracy(a,b,ax))
        [a, b, ax] = self.create_data_not_match()
        self.assertEqual(0, accuracy(a, b, ax))
        [a, b, ax] = self.create_data_half_match()
        self.assertEqual(0.5, accuracy(b, a, ax))

    def test_precision(self):
        [a, b, ax] = self.create_data_match()
        self.assertEqual(1, precision(a, b,0, ax))
        [a, b, ax] = self.create_data_not_match()
        self.assertEqual(1, precision(a, b,0, ax))
        [a, b, ax] = self.create_data_half_match()
        self.assertEqual(0.5, precision(b, a,0, ax))
        self.assertEqual(1, precision(a, b, 0, ax))

    def test_recall(self):
        [a, b, ax] = self.create_data_match()
        self.assertEqual(1, recall(a, b, 0, ax))
        [a, b, ax] = self.create_data_not_match()
        self.assertEqual(0, recall(a, b, 0, ax))
        [a, b, ax] = self.create_data_half_match()
        self.assertEqual(1, recall(b, a, 0, ax))
        self.assertEqual(0.5, recall(a, b, 0, ax))

    def test_IoU(self):
        [a, b, ax] = self.create_data_match()
        self.assertEqual(1, IoU(a, b,0, ax))
        [a, b, ax] = self.create_data_not_match()
        self.assertEqual(0, IoU(a, b,0, ax))
        [a, b, ax] = self.create_data_half_match()
        self.assertEqual(0.5, IoU(a, b,0, ax))


class Test3D(unittest.TestCase,TestMetrics):
    def create_data_match(self):
        a = np.zeros(shape=(10,1,2))
        b = np.zeros(shape=(10,1,2))
        a[:,0,0]=1
        b[:, 0,0] = 1
        return [a,b,2]

    def create_data_not_match(self):
        a = np.zeros(shape=(10,1,2))
        b = np.zeros(shape=(10,1,2))
        a[:,0,0]=1
        b[:, 0,1] = 1
        return [a,b,2]

    def create_data_half_match(self):
        a = np.zeros(shape=(10,1,2))
        b = np.zeros(shape=(10,1,2))
        a[:,0,0]=1
        b[:5, 0,1] = 1
        b[5:, 0, 0] = 1
        return [a,b,2]


class Test2D(unittest.TestCase,TestMetrics):
    def create_data_match(self):
        a = np.zeros(shape=(10,2))
        b = np.zeros(shape=(10,2))
        a[:,0]=1
        b[:, 0] = 1
        return [a,b,1]

    def create_data_not_match(self):
        a = np.zeros(shape=(10,2))
        b = np.zeros(shape=(10,2))
        a[:,0]=1
        b[:, 1] = 1
        return [a,b,1]

    def create_data_half_match(self):
        a = np.zeros(shape=(10,2))
        b = np.zeros(shape=(10,2))
        a[:,0]=1
        b[:5,0] = 1
        b[5:, 1] = 1
        return [a,b,1]

if __name__ == '__main__':
    unittest.main()
