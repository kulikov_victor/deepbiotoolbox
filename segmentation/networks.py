import keras
import tensorflow as tf
from keras import backend as K
from keras.layers import Dropout, Dense
from keras.layers import Input, MaxPooling2D, UpSampling2D, core, add, BatchNormalization, Conv2D, LeakyReLU
from keras.layers.merge import concatenate
from keras.models import Model
from sklearn.metrics import confusion_matrix

from custom_layers import PairwiseConv,SmoothConv
from data.augmentation import Augmentation
from data.sets import DeepBioData
from metrics import *


class SegNet:
    """
    Abstract class used as parent for all segmentation task in the toolbox.
    """

    def __init__(self, dataset, weights_file='', verbose=False, log_device_placement=False, sess=None):
        """
        
        :param dataset: 
        :param weights_file: path for save model storage
        :type weights_file: string
        :param verbose: 
        :param log_device_placement: tensorflow session configuration
        """
        assert isinstance(dataset, DeepBioData)
        self.dataset = dataset
        self.n_channels = dataset.ndims
        self.n_labels = dataset.nlabels
        self.model = None
        self.model_weights = None
        self.previous_shape = None
        self.verbose = verbose
        self.model_name = None
        self.weights_file = weights_file
        self.batch_size = 1
        self.batch_factor = 1
        self.graph = tf.Graph()

        if sess is None:
            config = tf.ConfigProto(log_device_placement=log_device_placement)
            self.sess = tf.Session(config=config)
            K.set_session(self.sess)

    def train(self, n_files, augmentations=[],
              steps_per_epoch=100, epochs=10, verbose=2, workers=6, callbacks=[],continue_training=False):
        """
        Train the inner network with using given dataset
        
        :param n_files: Number of files to process
        :type n_files: int
        :param augmentations: list of data augmentation used for training
        :type augmentations: list of Augmentation
        :param steps_per_epoch: Number of batches for each iteration
        :type steps_per_epoch: int
        :param epochs: Number of epochs
        :type epochs: int
        :param workers: Parallel generation of batches
        :type workers: int
        :param verbose: Show intermediate results
        :type verbose: int
        :return: dictionary of training results
        """

        # assert isinstance(session, tf.Session)
        assert isinstance(steps_per_epoch, int)
        assert isinstance(epochs, int)
        assert isinstance(workers, int)
        assert isinstance(continue_training,bool)

        for aug in augmentations:
            assert isinstance(aug, Augmentation)

        self.previous_shape = None

        self.dataset.augmentation = augmentations
        shape = self.dataset.augmented_shape()
        self.batch_size = n_files * shape[0] * self.batch_factor

        self.model = self.create_model(shape[1], shape[2])

        self.sess.run(tf.global_variables_initializer())

        #if nessary continue the training
        if continue_training:
            self.model.load_weights(self.weights_file + '.h5')

        with self.sess.as_default():
            self.model.fit_generator(self._create_train_generator(n_files), steps_per_epoch=steps_per_epoch,
                                     epochs=epochs, verbose=verbose, workers=workers, callbacks=callbacks)

        if self.weights_file:
            self.model.save_weights(self.weights_file + '.h5', overwrite=True)
        self.model_weights = self.get_model_weights(self.model)

    def test(self, augmentations=[], n_test=100, ax=2, random_seed=None):
        """
        Test the trained network on the dataset
        
        :param dataset: a instance of class DeepBioData
        :param augmentations: list of instances of augmentation algorithms  
        :param n_test: number of random
        :return: a dictionary of measured metrics
        """
        self.previous_shape = None
        self.dataset.augmentation = augmentations
        shape = self.dataset.augmented_shape()
        self.batch_size = shape[0]

        self.model = self.create_model(shape[1], shape[2])

        # try to load model weights
        if self.model_weights is not None:
            self.set_model_weights(self.model, self.model_weights)
        elif self.weights_file:
            self.model.load_weights(self.weights_file + '.h5')
        else:
            raise "Can't find model weights"

        test_generator = self._create_test_generator(1)

        # perform tests
        estimated_accuracy = 0
        estimated_precision = 0
        estimated_recall = 0
        estimated_iou = 0

        y_gt = None
        y_pred = None
        with self.sess.as_default():
            for i in xrange(n_test):
                [x, y] = test_generator.next()
                y_predicted = self.model.predict(x)
                if y_pred is None:
                    y_gt = y['y']
                    y_pred = y_predicted
                else:
                    y_gt = np.append(y_gt, y['y'], axis=ax - 1)
                    y_pred = np.append(y_pred, y_predicted, axis=ax - 1)

                estimated_accuracy += accuracy(y['y'], y_predicted, ax=ax)
                estimated_precision += mean_precision(y['y'], y_predicted, ax=ax)
                estimated_recall += mean_recall(y['y'], y_predicted, ax=ax)
                estimated_iou += mean_IoU(y['y'], y_predicted, ax=ax)

        # normalize to the number of tests
        estimated_accuracy /= n_test
        estimated_precision /= n_test
        estimated_recall /= n_test
        estimated_iou /= n_test

        # print(y_gt.shape, y_pred.shape)
        # print(y_gt, y_pred)
        gt_labels = np.argmax(y_gt, axis=ax)
        pred_labels = np.argmax(y_pred, axis=ax)
        # print(gt_labels.shape, pred_labels.shape)
        # print(gt_labels, pred_labels)
        if len(gt_labels.shape) > 1:
            # multiple rows
            conf = confusion_matrix(gt_labels[0], pred_labels[0])
        else:
            # single row
            conf = confusion_matrix(gt_labels, pred_labels)

        conf = conf.astype('float') / conf.sum(axis=1)[:, np.newaxis]

        return [estimated_accuracy, estimated_precision, estimated_recall, estimated_iou, conf]

    def process_image(self, image, probabilities=False):
        """
        Process one whole image
        
        :param image: the image to be processed
        :param probabilities: returns raw probabilities, of colors
        :return: 
        """
        assert self.dataset.ndims == image.shape[2]

        self.batch_size = 1

        if (self.previous_shape is None) or (self.previous_shape != image.shape):
            self.model = self.create_model(image.shape[0], image.shape[1])

            self.previous_shape = image.shape
            # try to load model weights
            if self.model_weights is not None:
                self.set_model_weights(self.model, self.model_weights)
            elif self.weights_file:
                self.model.load_weights(self.weights_file + '.h5')
            else:
                raise "Can't find model weights"

        image = (image - self.dataset.pixel_mean) / self.dataset.max_value
        image = image[np.newaxis, :, :, :]

        y_predicted = self.model.predict(image)
        y_predicted = y_predicted[0].reshape(image.shape[1], image.shape[2], self.dataset.nlabels)
        if not probabilities:
            return self.dataset.make_color(y_predicted)
        return y_predicted

    def _create_train_generator(self, n_files):
        return self.dataset.generate(n_files=n_files)

    def _create_test_generator(self, n_files):
        return self.dataset.generate_test(n_files=n_files)

    def create_model(self, patch_height, patch_width):
        raise "Not implemented"

    @staticmethod
    def get_model_weights(model):
        sz = len(model.layers)
        weights_list = []
        for k in range(sz):
            weights_list.append(model.layers[k].get_weights())
        return weights_list

    @staticmethod
    def set_model_weights(model, weights_list):
        sz = len(model.layers)
        assert sz == len(weights_list)
        for k in range(sz):
            model.layers[k].set_weights(weights_list[k])


class Unet(SegNet):
    """
    U-net like segmentation algorithm
    """

    def __init__(self, dataset, weights_file='', verbose=False):
        SegNet.__init__(self, dataset, weights_file, verbose)
        self.model_name = "Unet"

    def create_model(self, patch_height, patch_width):
        assert (0 == patch_height % 4 and 0 == patch_width % 4)  # otherwise we will get errors with concatenation

        inp = Input((patch_height, patch_width, self.n_channels), name='x')
        conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inp)
        conv1 = Dropout(0.2)(conv1)
        conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
        #
        conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
        conv2 = Dropout(0.2)(conv2)
        conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
        #
        conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
        conv3 = Dropout(0.2)(conv3)
        conv3 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv3)

        up1 = concatenate([UpSampling2D(size=(2, 2))(conv3), conv2])
        conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
        conv4 = Dropout(0.2)(conv4)
        conv4 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv4)
        #
        up2 = concatenate([UpSampling2D(size=(2, 2))(conv4), conv1])
        conv5 = Conv2D(32, (3, 3), activation='relu', padding='same')(up2)
        conv5 = Dropout(0.2)(conv5)
        conv5 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv5)
        #
        conv6 = Conv2D(self.n_labels, (1, 1), activation='relu', padding='same')(conv5)
        conv6 = core.Reshape((patch_height * patch_width, self.n_labels))(conv6)
        ############
        conv7 = core.Activation('softmax', name='y')(conv6)

        model = Model(inputs=inp, outputs=conv7)

        if self.verbose:
            model.summary()

        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.name = "U-net"
        return model


class IncN(SegNet):
    def __init__(self, dataset, weights_file='', layer_number=3, kernel_size=3, output_maps=8, verbose=False):
        SegNet.__init__(self, dataset, weights_file, verbose)
        self.model_name = "Inc"
        self.res = False
        self.kernel = (kernel_size, kernel_size)
        self.layer_number = layer_number
        self.output_maps = output_maps

    def bottleneckmodule(self, input_data, res=False):
        # dilation_rate
        C1 = Conv2D(self.output_maps, self.kernel, padding='same')(input_data)
        C3 = Conv2D(self.output_maps, self.kernel, dilation_rate=(3, 3), padding='same')(input_data)
        C5 = Conv2D(self.output_maps, self.kernel, dilation_rate=(5, 5), padding='same')(input_data)
        C7 = Conv2D(self.output_maps, self.kernel, dilation_rate=(7, 7), padding='same')(input_data)

        M = concatenate([C1, C3, C5, C7])
        M = LeakyReLU(0.2)(M)
        E = BatchNormalization()(M)
        D = Dropout(0.2)(E)
        if res:
            X = Conv2D(self.output_maps * 4, (1, 1), activation='relu', padding='same')(input_data)
            D = add([D, X])
        return D

    def create_model(self, patch_height, patch_width):
        inp = Input((patch_height, patch_width, self.n_channels), name='x')
        E = self.bottleneckmodule(inp)
        for i in range(0, self.layer_number):
            E = self.bottleneckmodule(E, res=self.res)
        Y = self.bottleneckmodule(E, res=self.res)

        X = Conv2D(self.n_labels, (1, 1), activation='relu', padding='same')(Y)
        X = core.Reshape((patch_height * patch_width, self.n_labels))(X)
        ############
        X = core.Activation('softmax', name='y')(X)

        model = Model(inputs=inp, outputs=X)
        if self.verbose:
            model.summary()
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.name = ""
        if self.res:
            model.name = model.name + "Res_"
        model.name = model.name + "Inception_" + str(self.layer_number)
        return model

class IncResN(IncN):
    def __init__(self, dataset, weights_file='', layer_number=3, kernel_size=3, output_maps=8, verbose=False):
        IncN.__init__(self, dataset, weights_file, layer_number, kernel_size, output_maps, verbose)
        self.model_name = "IncRes"
        self.res = True


class SmoothIncN(IncN):
    """
    Inc-N with smoothed dilated convolution before dilated convolution using a 
    smooth filter with the same size that dilatation step
    """
    def __init__(self, dataset, weights_file='', layer_number=3, kernel_size=3, output_maps=8, verbose=False,smooth_class=None):
        IncN.__init__(self, dataset, weights_file, layer_number, kernel_size, output_maps, verbose)
        self.model_name = "SmoothInc"
        if smooth_class is None:
            self.smooth_class = SmoothConv
        else:
            self.smooth_class = smooth_class

    def bottleneckmodule(self, input_data, res=False):
        # dilation_rate
        num = input_data.shape[3]

        C1 = Conv2D(self.output_maps, self.kernel, padding='same')(input_data)
        A1 = self.smooth_class(3)(input_data)
        C3 = Conv2D(self.output_maps, self.kernel, dilation_rate=(3, 3), padding='same')(A1)
        A2 = self.smooth_class(5)(input_data)
        C5 = Conv2D(self.output_maps, self.kernel, dilation_rate=(5, 5), padding='same')(A2)
        A3 = self.smooth_class(7)(input_data)
        C7 = Conv2D(self.output_maps, self.kernel, dilation_rate=(7, 7), padding='same')(A3)

        M = concatenate([C1, C3, C5, C7])
        M = LeakyReLU(0.2)(M)
        E = BatchNormalization()(M)
        D = Dropout(0.2)(E)
        if res:
            X = Conv2D(self.output_maps * 4, (1, 1), activation='relu', padding='same')(input_data)
            D = add([D, X])
        return D


class SmoothIncResN(SmoothIncN):
    def __init__(self, dataset, weights_file='', layer_number=3, kernel_size=3, output_maps=8, verbose=False,smooth_class=None):
        SmoothIncN.__init__(self, dataset, weights_file, layer_number, kernel_size, output_maps, verbose,smooth_class)
        self.model_name = "SmoothIncRes"
        self.res = True


class AdaptedIncN(IncN):
    def __init__(self, dataset, weights_file='', layer_number=3, kernel_size=3, output_maps=8,
                 verbose=False, n_adapter_layers=None):
        # Check we have dataset with skips management

        IncN.__init__(self, dataset, weights_file, layer_number, kernel_size, output_maps, verbose)
        if n_adapter_layers is None:
            self.n_adapter_layers = dataset.ndims
        else:
            self.n_adapter_layers = n_adapter_layers

    def get_intermediate_layer(self, image, active_channels):
        """
        get the intermediate representation of input image with missing channels
        :param image: image to be processed (WxHxC)
        :param active_channels: list of 
        :return: 
        """
        assert self.model is not None
        assert isinstance(image, np.ndarray)
        assert isinstance(active_channels, list)
        assert len(active_channels) == image.shape[2]

        image = (image - self.dataset.pixel_mean) / self.dataset.max_value
        image = image[np.newaxis, :, :, :]

        feed_dict = {self.model.get_layer('x').output: image,
                     self.model.get_layer('c').output: np.array(active_channels)}
        return self.sess.run([self.model.get_layer('intermediate').output], feed_dict=feed_dict)

    def process_image(self, image, cl, probabilities=False):
        """
        Process one whole image

        :param image: the image to be processed
        :param probabilities: returns raw probabilities, of colors
        :return: 
        """
        assert self.dataset.ndims == image.shape[2]

        self.batch_size = 1

        if (self.previous_shape is None) or (self.previous_shape != image.shape):
            self.model = self.create_model(image.shape[0], image.shape[1])
            self.previous_shape = image.shape
            # try to load model weights
            if self.model_weights is not None:
                self.set_model_weights(self.model, self.model_weights)
            elif self.weights_file:
                self.model.load_weights(self.weights_file + '.h5')
            else:
                raise "Can't find model weights"

        maxvals = cl[0] * self.dataset.max_value
        maxvals[0 == maxvals] = 1
        image = (image - cl[0] * self.dataset.pixel_mean) / maxvals
        image = image[np.newaxis, :, :, :]

        y_predicted = self.model.predict([image, cl])
        y_predicted = y_predicted[0].reshape(image.shape[1], image.shape[2], self.dataset.nlabels)
        if not probabilities:
            return self.dataset.make_color(y_predicted)
        return y_predicted

    def create_model(self, patch_height, patch_width):
        data_in = Input((patch_height, patch_width, self.n_channels), name='x')
        active_channels = Input((self.n_channels,), name='c')
        active_pre = Dense(self.n_channels * self.n_adapter_layers, activation='relu', use_bias=False)(active_channels)
        active_flat = core.Reshape((1, self.n_channels, self.n_adapter_layers))(active_pre)
        inp = PairwiseConv(self.batch_size)([data_in, active_flat])
        inp = LeakyReLU(0.1, name='intermediate')(inp)
        e = self.bottleneckmodule(inp)
        for i in range(0, self.layer_number):
            e = self.bottleneckmodule(e, res=self.res)
        y = self.bottleneckmodule(e, res=self.res)

        y = Conv2D(self.n_labels, (1, 1), activation='relu', padding='same')(y)
        y = core.Reshape((patch_height * patch_width, self.n_labels))(y)
        ############
        y = core.Activation('softmax', name='y')(y)

        model = Model(inputs=[data_in, active_channels], outputs=y)
        if self.verbose:
            model.summary()
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.name = "Adapted_"
        if self.res:
            model.name = model.name + "Res_"
        model.name = model.name + "Inception_" + str(self.layer_number)
        return model


class AdaptedResIncN(AdaptedIncN):
    """
    Same as AdaptedIncN but with residual link
    """
    def __init__(self, dataset, weights_file='', layer_number=3, kernel_size=3, output_maps=8, verbose=False,
                 n_adapter_layers=None):
        # Check we have dataset with skips management
        AdaptedIncN.__init__(self, dataset, weights_file, layer_number, kernel_size, output_maps, verbose,
                             n_adapter_layers)
        self.res = True
