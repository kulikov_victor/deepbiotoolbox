import unittest

from data.augmentation import ClipPatches, ElasticTransform, Affine
from data.sets import Hoechst
from networks import Unet, IncN, IncResN

class TestNetwork:
    def create_network(self):
        raise "Not implemented"

    def test_train(self):
        net = self.create_network()
        net.train(2, augmentation_list, steps_per_epoch=10, epochs=1)

    def test_testing(self):
        net = self.create_network()
        print(net.test(augmentation_list))

    def test_fullimage(self):
        net = self.create_network()
        net.process_image(test_dataset.test[0])


class TestUnet(unittest.TestCase, TestNetwork):

    def create_network(self):
        return Unet(test_dataset, weights_file="../models/Unet_hoechst")


class TestInc(unittest.TestCase, TestNetwork):
    def create_network(self):
        return IncN(test_dataset, weights_file="../models/Inc_hoechst")


class TestIncResN(unittest.TestCase, TestNetwork):
    def create_network(self):
        return IncResN(test_dataset, weights_file="../models/IncRes_hoechst")


if __name__ == '__main__':
    test_dataset = Hoechst(0.9)
    augmentation_list = [Affine(), ClipPatches(), ElasticTransform()]
    unittest.main()
